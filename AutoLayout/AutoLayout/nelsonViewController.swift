//
//  nelsonViewController.swift
//  AutoLayout
//
//  Created by Nelson Chicaiza on 4/7/18.
//  Copyright © 2018 Carlos Osorio. All rights reserved.
//

import UIKit

class nelsonViewController: UIViewController {

    
    @IBOutlet var amarrilloView: UIView!
    
    @IBOutlet var celesteView: UIView!
    
    @IBOutlet var azulView: UIView!
    
    
    @IBOutlet var rojoView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func amarilloTap(_ sender: Any) {
        amarrilloView.backgroundColor = .red
    }
    
    
    @IBAction func celesteTap(_ sender: Any) {
        celesteView.backgroundColor = .white
    }
    
    
    @IBAction func azulTap(_ sender: Any) {
        azulView.backgroundColor = .green
    }
    
    
    @IBAction func rojoTap(_ sender: Any) {
        rojoView.backgroundColor = .black
        
    }
}
