//
//  TapViewController.swift
//  AutoLayout
//
//  Created by Nelson Chicaiza on 27/6/18.
//  Copyright © 2018 Carlos Osorio. All rights reserved.
//

import UIKit

class TapViewController: UIViewController{
    
    
    @IBOutlet var amarillo: UIView!
    @IBOutlet var rojo: UIView!
    @IBOutlet var azul: UIView!
    @IBOutlet var celeste: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func tap01(_ sender: Any) {
        amarillo.backgroundColor = .red
    }
    
    @IBAction func tap02(_ sender: Any) {
        celeste.backgroundColor = .black
    }
    
    @IBAction func tap03(_ sender: Any) {
        azul.backgroundColor = .white
    }
    
    
    @IBAction func tap04(_ sender: Any) {
        rojo.backgroundColor = .yellow
    }
    
    
}
