//
//  saveViewController.swift
//  AutoLayout
//
//  Created by Nelson Chicaiza on 10/7/18.
//  
//

import UIKit

class saveViewController: UIViewController {

    
    @IBOutlet var messageLabel: UILabel!
    
    @IBOutlet var messageTextField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let message:String? = UserDefaults.standard.value(forKey: "message") as? String
        messageLabel.text = message
        
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        messageLabel.text = messageTextField.text
        UserDefaults.standard.set(messageTextField.text, forKey: "message")
        
    }

    
    

   
}
